This work is licensed under a Creative Commons Attribution-ShareAlike 2.0 Generic License.
http://creativecommons.org/licenses/by-sa/2.0/
see also the COPYING file.

The plan is to create files for each of the Municipalites of Kosovo and after that for each major city.
http://en.wikipedia.org/wiki/Municipalities_of_Kosovo

my last attempt with that simple perl script was not that good
http://gitorious.org/osmgit/osmgit-test/trees/logistics-plus-update4
http://osmopenlayers.blogspot.com/2010/06/first-running-of-osm-gittorrent.html

also using tiles for splitting is not optimal 
people want a structure that is usable, and logical, human
better to put the work into a human directory strucure
so you break the map up into small chunks that can be loaded in josm
each one is just a document, and you edit them with josm
and that is it, no database, no stress. rendering via mapnik or something when they are checked in via git hooks
the webpages with browsing information can also be generated statically upon checkin
at least for kosovo it is so small, i mean you dont need much 

For now I am using this source : 
  wget http://downloads.cloudmade.com/europe/southern_europe/kosovo/kosovo.osm.bz2
  wget http://downloads.cloudmade.com/europe/southern_europe/kosovo/kosovo.poly

The first job is to create polygons for all the areas of interest to do a better and intelligent splitting, the OSM data alreay is pretty good and can be used for this.

to get the scripts for polygons :
svn co http://svn.openstreetmap.org/applications/utils/osm-extract/polygons 

read more here :
* http://wiki.openstreetmap.org/wiki/Osmosis/Polygon_Filter_File_Format#Converting_To.2FFrom_OSM_Format and here http://wiki.openstreetmap.org/wiki/User:Davetoo/GIS/DataExchange/Polygons 
* http://wiki.openstreetmap.org/wiki/Polygon
* http://trac.openstreetmap.org/browser/applications/utils/osm-extract/polygons/README

Here are the instructions on what to do :

First I build the osmsis from source :
see also http://wiki.openstreetmap.org/wiki/Osmosis/Installation

 svn co http://svn.openstreetmap.org/applications/utils/osmosis/trunk/ osmosis
 cd osmosis/
  sudo apt-get install ant
  sudo apt-get install openjdk-6-jdk
  ant publish

then you unpack the zip file and use it

  unzip osmosis/./package/distrib/zips/osmosis-SNAPSHOT-r25892.zip
  cd osmosis-SNAPSHOT-r25892/

To get the data extract of all kosovo :


Here are some more things to read  :
http://wiki.openstreetmap.org/wiki/Osmosis#Extracting_bounding_boxes

To extract admin level 4, the municipalities  :
  "bash ./osmosis --read-xml ../../../KosovoCCOpenStreetMap/gitrepo/kosovo.osm  --tf accept-ways admin_level=4  --used-node --wx admin4.osm"
  
bash ./osmosis --read-xml ../../../KosovoCCOpenStreetMap/gitrepo/kosovo.osm  --tf accept-ways admin_level=2  --used-node --wx admin2.osm


Now the most important thing is this,
the Klina_admin.osm was produced like this,
From the level 4 admin layers, I copied Klina out, then pasted it into a new osm file in josm. Then from there, I removed all attributed, merged the ways, removed the relation and made it all go into one direction. this creates a simple polygon. 

perl osm2poly.pl Klina_admin.osm  > Klina_admin.poly

To create the klines file :
  bash ./osmosis --read-xml file=kosovo.osm --bounding-polygon file="Klina_admin.poly" --write-xml file="Klina.osm"

An example first commit of Klina is here :
http://repo.or.cz/w/KosovoCCOpenStreetMap.git/commit/8d2fc0d4d3190215edeabf0ba6ee033adfdf34d1


If you want to help, please produce some new polygons, you can register a user here http://repo.or.cz/reguser.cgi and help in the project directly, or put your clone anywhere on the internet with git. 
