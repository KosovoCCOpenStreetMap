#!/usr/bin/python2.6
# based on http://trac.mapnik.org/wiki/XMLGettingStarted
# http://wiki.openstreetmap.org/wiki/Mapnik:_Rendering_OSM_XML_data_directly
from mapnik import *

mapfile = 'Klina_mapnik_border_style.xml'
map_output = 'Klina_mapnik_border.png'

m = Map(4*1024,4*1024)
load_map(m, mapfile)
# minx, miny , maxx, maxy
bbox=(Envelope( 20.459120, 42.519180,  20.764920, 42.716530 ))
#bbox=(Envelope( 20, 42,  21, 43 ))
#bbox = Envelope(Coord(-180.0, -90.0), Coord(180.0, 90.0))
m.zoom_to_box(bbox)
print "Scale = " , m.scale()
render_to_file(m, map_output)
