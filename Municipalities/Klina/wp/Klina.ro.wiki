{{Casetă așezare
|nume = Klina / Klinë </br> Клина / Klina
|hartă = Klinë 2006.PNG
|descriere_hartă = Localizare Klina
|latd=42 |latm=37 |lats=18 |latNS=N
|longd=20 |longm=34 |longs=40 |longEV=E
|lider_titlu = Primar
|lider_nume = Prekë Gjeta
|suprafață_totală_km2 = 308
|altitudine = 550
|recensământ = 1991
|populație = 65,033 (municipiu)
|densitate = 177.6
|cod_poștal = 32000
|prefix_telefonic = +381 (0)39
|fus_orar = CET
|utc_offset = +1
|tip_subdiviziune = District
|nume_subdiviziune = [[Districtul Peć|Peć]]
|sit-adresă = http://www.komuna-kline.org
|sit-nume = Municipiul Klina
|sit-limbă = sq
}}

'''Klina''' sau '''Klinë''' ({{sq|Klina sau Klinë}}; {{sr|Клина, Klina}}; {{gr|Chynna, Χυννα}}) este un oraș și municipiu în centrul proviniciei [[Kosovo]], făcând parte din [[districtul Peć]]. Este așezat la confluența [[râul Klina|râului Klina]] cu [[râul Drinul Alb]].

==Istorie==
Klina este identificat cu o așezare a [[Iliri|iliricilor]] din ''Chinna'', menționat de [[Ptolemeu]] în [[Geografia lui Ptolemeu|geografia lui]] (Cartea II, Capitolul 15)

==Economie==
Cea mai mare companie de servicii private în [[Klina]] este "Albini", administrând o rețea mare de supermarket-uri.

==Stemă==
O stemă în Klina este [[Cascada]] [[Mirusa]].

==Demografie==
{|border="2" cellspacing="0" cellpadding="4" rules="all" width="65%" style="clear:all; margin:5px 0 0em 0em; border-style: solid; border-width: 1px; border-collapse:collapse; font-size:85%; empty-cells:show"
|colspan="14" align=center style="background:#778899; color:white"|'''Structura etnică'''
|- bgcolor="#FFEBCD"
!Populație/An
!Albanezi
!&nbsp;%
!Sârbi
!&nbsp;%
!Muntenegrii
!&nbsp;%
!Romi
!&nbsp;%
!Total
|- bgcolor="#fffaf0"
|1961 || 18,124|| 66.75|| 7,378|| 27.17|| 1,372|| 5.05|| 80|| 0.29||27,153
|- bgcolor="#f5f5f5"
|1971 || 33,050|| 78.04|| 7,864|| 18.57|| 1,157|| 2.73|| 118|| 0.28||42,351
|- bgcolor="#fffaf0"
|1981 || 45,594|| 83.60|| 6,829|| 12.52|| 973|| 1.78|| 798|| 1.46||54,539
|- bgcolor="#f5f5f5"
|1991 || 43,248|| 82.75|| 5,209|| 9.97|| 621|| 1.19|| 1,278|| 2.45||52,266
|- bgcolor="#fffaf0"
|Ianuarie 1999|| 55,000|| 78.6|| 10,000|| 14.3|||||| 5,000|| 7.1|| 70,000   
|- bgcolor="#f5f5f5"
|2006 ||53,000|| 96.5|| 94|| 0.17|||||| 1,800|| 3.3|| 54,900 
|- 
|colspan="10" align=center style="background:#dcdcdc;"|<small>Ref: Recensământul populației Iugoslavia din anul 1991, estimările [[OSCE]] pentru anii 1999 și 2006 </small>'''
|}

==Vezi și==
* [[Districtul Peć]]

==Referințe==

==Legături externe==
* [http://www.komuna-kline.org/ Municipiul Klina]
* [http://www.osce.org/documents/html/pdftohtml/1188_en.pdf.html OSCE Profilul municipiului Klina]
* [http://www.sok-kosovo.org/pdf/population/Kosovo_and_its_population.pdf SOK Kosovo și populația provinciei]

{{Municipiile din Kosovo}}

[[Categorie:Orașe în Kosovo]]
[[Categorie:Municipiile din Kosovo]]

[[de:Klina]]
[[en:Klina]]
[[fr:Klina]]
[[hr:Klina]]
[[it:Klina]]
[[nl:Klinë]]
[[pl:Klina]]
[[sh:Klina]]
[[sq:Klina]]
[[sr:Клина]]
[[tr:Klina]]