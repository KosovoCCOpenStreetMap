{{S|Kosovo}}
{{Città
|nomeCitta =Klinë
|nomeOriginale=Klinë<br/>Клина/Klina
|linkBandiera =
|linkStemma =	
|panorama =
|linkMappa =Klinë 2006.PNG
|pxMappa =
|stato = KOS
|suddivisioneAmministrativa1= [[Distretto di Peja|Peja]]
|suddivisioneAmministrativa2= Klinë
|suddivisioneAmministrativa3= 
|suddivisioneAmministrativa4=
|suddivisioneAmministrativa5=
|latitudine_d =	
|longitudine_d =
|latitudineGradi = 42
|latitudinePrimi = 37
|latitudineNS =N
|longitudineGradi = 20
|longitudinePrimi = 35
|longitudineEW =E
|altitudine =382
|superficie = 308
|abitanti = 65.033 
|anno = 1991
|densità =211,15
|cap =32000
|prefisso =+381 39 
|targa =
|nomeAbitanti =
|status =
|sindaco =Sokol Bashota                                                                                                          |lingua =
|fuso =UTC+1
|notemappa =
|note = <sup>1</sup>Sotto protettorato [[Organizzazione delle Nazioni Unite|ONU]], dichiaratosi unilateralmente Repubblica indipendente (riconosciuta da [[Relazioni internazionali del Kosovo|alcuni stati]]), seccessionista dalla [[Serbia]] secondo cui è una Provincia autonoma
|sito =
}}

'''Klinë''' (in [[lingua albanese|albanese]] ''Klinë''; in [[lingua serba|serbo]]: Клина / ''Klina'') è una città del [[Kosovo]].

==Storia==
Klinë è identificata con l'insediamento [[Illiri|illirico]] di ''Chinna'', menzionato da [[Claudio Tolomeo|Tolomeo]] nella sua ''Geographia'' (Libro II, capitolo 15).

==Simboli==
Simbolo della municipalità sono le cascate Mirusha.

==Divisione amministrativa==
La municipalità si divide nelle seguenti frazioni e villaggi:

Balince, Berkovo, Biča, Bobovac, Bokshiq, Budisalc, Gjurgjevikë i madh,Krusheva e Madhe, Vidajë, Vlaški Drenovac, Valljak, Vrmnica, Golubovac, Gornji Petrič, Grabanicë, Grabac, Gremnikë, Deič, Ujmirë, Dobri Dol, Dollc, Dellovë, Donji Petrič, Drenofc, Drenovčić, Dresnik, Dugonjive, Dush, Dušević, Zabrđe, Zajm, Zllakuqan, Gllarevë, Jagoda, Jellofc, Jashanicë, Kievë, Klinë, Klinafc, Kpuz, Krnjince, Leskovac, Llozicë, Mali Đurđevik, Malo Kruševo, Mlečane, Naglavci, Pločice, Pogragjë, Përcev, Radulovac, Renovac, Resnik, Rudice, Svrhe, Siqevë, Skorošnik, Stup, Cerovik, Crni Lug, Qabic, Qeskovë, Qupevë e Shtupel.

== Altri progetti ==
{{interprogetto|commons=Category:Klina}}

{{Municipalità del Kosovo}}

[[Categoria:Città del Kosovo]]

[[de:Klina]]
[[en:Klina]]
[[fr:Klina]]
[[hr:Klina]]
[[nl:Klinë]]
[[pl:Klina]]
[[ro:Klina]]
[[sh:Klina]]
[[sq:Klina]]
[[sr:Клина]]
[[tr:Klina]]